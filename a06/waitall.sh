#!/bin/sh

[ "$#" -ne 1 ] && { echo "Usage: ./waitall.sh [:digit:]"; exit 2; }

# run child processes in bg and store their pids($!) in array
for i in $( seq 1 $1 ); do
    ./child.sh &
    pids[${i}]=$!
done

killchildren(){
    for pid in ${pids[*]}; do
        kill -s TERM $pid
    done
} 2> /dev/null

Status=0

trap 'killchildren; exit "$Status"' EXIT  # kill all children and exit with the current status

failurejobs=""
# wait for all children using their pids, recording failurejobs along the way 
for pid in ${pids[*]}; do
    wait $pid || { failurejobs="$pid $failurejobs" && Status=1; } 
done

successjobs=""
# filter success jobs from failure jobs
for pid in ${pids[*]}; do
    echo $failurejobs | grep -q $pid && continue # skip if this pid is a failure
    successjobs="$pid $successjobs" # otherwise append to successfull Jobs
done

echo "Successes: $successjobs"
echo "Failures: $failurejobs" 

exit $Status
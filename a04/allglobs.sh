#!/bin/sh

[ $# -ge 1 ] || {
cat <<- USAGE
Usage: $(basename $0) glob...
USAGE
exit 1
} >&2

while IFS=""; read line; do
        case "$line" in
            $*) printf '%s\n' "$line"; status=0;;
        esac
done


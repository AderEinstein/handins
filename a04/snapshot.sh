#!/bin/sh

source="$PWD"
snapdir="$PWD/.snapshot"

if [ ! -d "$snapdir" ]; then
    mkdir -v $snapdir
fi

cd $snapdir

if [ -d $snapdir/0 ]; then
    if [ -d $snapdir/9 ]; then
        cd $snapdir/9
        rm -fv $snapdir/9/*
        cd $snapdir
        rmdir -v $snapdir/9
    fi
    for dir in $( seq 8 -1 0); do
        if [ -d $snapdir/$dir ]; then
            mv -v $snapdir/$dir $snapdir/$(( $dir + 1 ))
        fi
    done
fi

mkdir -v "$snapdir/0"
cp -v $source/* $snapdir/0
cd $source
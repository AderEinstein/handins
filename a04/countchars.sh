#!/bin/sh

[ $# -ge 1 ] || {
cat <<- USAGE
Usage: $(basename $0) glob...
USAGE
exit 1
} >&2

while IFS=""; read line; do
    for char in "$@"; do
        printf "$( echo "$line" | tr -dc "$char" | wc -c ) "
    done
    echo
done


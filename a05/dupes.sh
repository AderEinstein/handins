#!/bin/sh


[ "$#" -gt "0" ] || set *

files=$( echo $@ )

hasDupes() {
        currfile=$1
        for file in $files; do
            [ "$currfile" = "$file" ] && continue 
            { [ -d "$file" ] || [ -d "$1" ]; } && continue #Do not compare a file with itself or with a directory
            
            [ $(cat $file | wc -c) -gt 0 ] && [ $(cat $1 | wc -c) -gt 0 ] && [ "$( cmp "$1" "$file" | wc -l )" -eq "0" ] && dupes="$dupes:$file"  #Append duplicate files to dupes
        done
        [ -n $dupes ] && return 0 || return 1
}

status=0
while [ $# -gt 0 ] ; do
    dupes=""   #reinitialize dupes for every file
    [ -f $1 ] && { hasDupes $@ && echo $1$dupes && status=1; } || { echo ":$1: is not a file" && status=2; }  
    shift
done

exit $status

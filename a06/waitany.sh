#!/bin/sh

[ "$#" -ne 1 ] && { echo "Usage: ./waitany.sh [:digit:]"; exit 2; }

trap 'killchildren ; exit 0' USR1
trap 'killchildren' EXIT

# run child processes in bg and store their pids($!) in array
for i in $( seq 1 $1 ); do
    ( 
        ./child.sh &
        trap 'kill $!' EXIT
        wait $! && kill -s USR1 $$ 
    ) &
    pids[${i}]=$!
done 2> /dev/null

parent=$$

killchildren(){
    for pid in ${pids[*]}; do
        #echo $pid ; #ps
        kill -s TERM $pid
    done
} 2> /dev/null

wait


echo "All failed"; exit 1
